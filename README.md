# Ryanjh
Ryanjh is a repo containing the code for my own personal [website](https://ryanjh.com/). In this README I documented what I did for my own personal records, as well as a place for recruiters to see my abilities.

## About
* Domain was purchased from [NameCheap](https://namecheap.com).
* Server is hosted by [Linode](https://linode.com).
  * Shout out to Linode's amazing [guides](https://www.linode.com/docs/guides/) that helped me along the way.
* Server is running a [LAMP](https://www.linode.com/docs/guides/how-to-install-a-lamp-stack-on-debian-10/) stack on Debian.
  * **Note**: I am using [MariaDB](https://mariadb.org/) instead of [MySQL](https://www.mysql.com/) as the guide does.
* SSL certificates were installed from [Let's Encrypt](https://letsencrypt.org) through the use of [Certbot](https://certbot.eff.org).
 
## Contributing
This section may evolve but as of April 7th, 2021 there is no CONTRIBUTING.md nor contributions that will be accepted.

## License
This section may evolve but as of April 7th, 2021 there is no LICENSE.md nor any license for the project.
